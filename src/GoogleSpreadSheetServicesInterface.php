<?php

namespace Drupal\googlespreadsheets;

interface GoogleSpreadSheetServicesInterface {

  public function getGSheetsTitle();
}
