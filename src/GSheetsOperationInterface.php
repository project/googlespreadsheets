<?php

namespace Drupal\googlespreadsheets;

interface GSheetsOperationInterface {

  public function getAllGsheetsData();
}
