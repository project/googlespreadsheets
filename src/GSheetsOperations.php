<?php

namespace Drupal\googlespreadsheets;

use Drupal\Core\Database\Connection;

class GSheetsOperations implements GSheetsOperationInterface {

  /*
   * @var \Drupal\Core\Database\Connection $database
   */
  protected $database;

  /**
   * Constructs a new GSheetsOperations object.
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(Connection $connection) {
    $this->database = $connection;
  }

  public function getAllGsheetsData() {
    $data = $this->database->select('googlespreadsheets', 'gs')
      ->fields('gs', array('gid'))
      ->execute()->fetchAll();

    return $data;
  }
}
