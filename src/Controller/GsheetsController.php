<?php

namespace Drupal\googlespreadsheets\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;

class GsheetsController extends ControllerBase {

  protected $gsheetsOperations;

  public function gsheetsOverview(Request $request) {

    $header = [];
    $header[] = ['data' => $this->t('Google Sheets Title'), 'field' => 'title', 'sort' => 'asc'];
    $header[] = $this->t('Operations');

    $rows = [];
    $gsheetService = \Drupal::service('googlespreadsheets.gsheetoperations');
    $sheet_ids = $gsheetService-> getAllGsheetsData();
    foreach ($sheet_ids as $sheet_id) {
      $row = [];
      $row['data']['sheet_id'] = $sheet_id->gid;

      $operations = [];
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('googlespreadsheets.admin_edit_gsheets', ['gid' => $sheet_id->gid]),
      ];
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('googlespreadsheets.admin_delete_gsheets', ['gid' => $sheet_id->gid]),
      ];

      $row['data']['operations'] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];

      $rows[] = $row;
    }

    $build['path_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No Google Sheets available. <a href=":link">Add Google Sheet</a>.', [':link' => $this->url('googlespreadsheets.admin_add_gsheets')]),
    ];

    return $build;
  }

  public function getGsheetsData() {

  }

}
