<?php

namespace Drupal\googlespreadsheets\Form;


use Drupal\Core\Form\FormStateInterface;

class AddGoogleSheetForm extends GoogleSpreadSheetsFormBase {

  public function getFormId() {
    return 'google_sheet_add';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['google_spread_sheet_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Spread Sheet\'s Id'),
      '#maxlength' => 255,
      '#size' => 45,
      '#description' => $this->t('Enter Google Spread Sheet Id'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gsheet_id = $form_state->getValue('google_spread_sheet_id');
    try {
      \Drupal::database()->insert('googlespreadsheets')
        ->fields([
          'gid' => $gsheet_id,
        ])->execute();
    }
    catch (\Exception $exception) {
    }
    $form_state->setRedirect('googlespreadsheets.gsheets');
  }
}
