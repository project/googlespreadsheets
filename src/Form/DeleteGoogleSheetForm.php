<?php

namespace Drupal\googlespreadsheets\Form;


use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class DeleteGoogleSheetForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_sheet_delete';
  }

  protected function getGid() {
    return \Drupal::request()->get('gid');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete Google Spread Sheet %title?', ['%title' => 'test']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('googlespreadsheets.gsheets');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $pid = NULL) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gid = $this->getGid();
    try {
      $deleted = \Drupal::database()->delete('googlespreadsheets')
        ->condition('gid', $gid)
        ->execute();
    }
    catch (\Exception $exception) {
    }
    $form_state->setRedirect('googlespreadsheets.gsheets');
  }

}
