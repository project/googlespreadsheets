<?php

namespace Drupal\googlespreadsheets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class GoogleSpreadSheetsSettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'googlespreadsheets_oauth_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'googlespreadsheets.oauth',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('googlespreadsheets.oauth');

    $form['client_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Client Id'),
      '#default_value' => $config->get('client_id'),
    );

    $form['project_id'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Project Id'),
      '#default_value' => $config->get('project_id'),
    );

    $form['auth_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Auth URI'),
      '#default_value' => $config->get('auth_uri'),
    );

    $form['token_uri'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Token URI'),
      '#default_value' => $config->get('token_uri'),
    );

    $form['auth_provider_x509_cert_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Auth Provider\'s Cert URL'),
      '#default_value' => $config->get('auth_provider_x509_cert_url'),
    );

    $form['client_secret'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret Key'),
      '#default_value' => $config->get('client_secret'),
    );

    $form['redirect_uris'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URIs'),
      '#default_value' => $config->get('redirect_uris'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
     $this->configFactory->getEditable('googlespreadsheets.oauth')
    // Set the submitted configuration setting
    ->set('client_id', $form_state->getValue('client_id'))
    ->set('project_id', $form_state->getValue('project_id'))
    ->set('auth_uri', $form_state->getValue('auth_uri'))
    ->set('token_uri', $form_state->getValue('token_uri'))
    ->set('auth_provider_x509_cert_url', $form_state->getValue('auth_provider_x509_cert_url'))
    ->set('client_secret', $form_state->getValue('client_secret'))
    ->set('redirect_uris', $form_state->getValue('redirect_uris'))
    ->save();

    parent::submitForm($form, $form_state);
  }
}
