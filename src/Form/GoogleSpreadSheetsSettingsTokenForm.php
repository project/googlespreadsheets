<?php

namespace Drupal\googlespreadsheets\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class GoogleSpreadSheetsSettingsTokenForm extends ConfigFormBase{
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'googlespreadsheets_token_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'googlespreadsheets.token',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('googlespreadsheets.token');

    $form['access_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#default_value' => $config->get('access_token'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration
    $this->configFactory->getEditable('googlespreadsheets.token')
      // Set the submitted configuration setting
      ->set('access_token', $form_state->getValue('access_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}

