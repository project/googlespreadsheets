<?php

namespace Drupal\googlespreadsheets\Form;

use Drupal\Core\Form\FormStateInterface;

class EditGoogleSheetForm extends GoogleSpreadSheetsFormBase {

  public function getFormId() {
    return 'google_sheet_edit';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['google_spread_sheet_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Spread Sheet\'s Id'),
      '#maxlength' => 255,
      '#size' => 45,
      '#default_value' => $this->getGid(),
      '#description' => $this->t('Enter Google Spread Sheet Id'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $gsheet_id = $form_state->getValue('google_spread_sheet_id');
    try {
      $num_updated = \Drupal::database()->update('googlespreadsheets')
        ->fields([
          'gid' => $gsheet_id,
        ])
        ->condition('gid', $this->getGid(), '=')
        ->execute();
    }
    catch (\Exception $exception) {
    }
    $form_state->setRedirect('googlespreadsheets.gsheets');
  }
}
