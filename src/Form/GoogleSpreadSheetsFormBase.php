<?php

namespace Drupal\googlespreadsheets\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

abstract class GoogleSpreadSheetsFormBase extends FormBase {

  /**
   * {@inheritdoc}
   */
  abstract function buildForm(array $form, FormStateInterface $form_state);

  /**
   * {@inheritdoc}
   */
  abstract function submitForm(array &$form, FormStateInterface $form_state);

  protected function getGid() {
    return \Drupal::request()->get('gid');
  }
}
